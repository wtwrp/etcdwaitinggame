use std::{collections::HashMap, sync::Arc};

use bytes::Bytes;
use color_eyre::{eyre::bail, Result};
use etcd_client::{EventType, GetOptions, PutOptions, WatchFilterType, WatchOptions};
use tokio::sync::{
    mpsc::{Receiver, Sender},
    oneshot, Mutex,
};
use tokio_stream::StreamExt;
use uuid::Uuid;

use crate::backend::{Backend, Input, Subscription};

struct Node {
    tx: Sender<Subscription>,
}

impl Node {
    pub async fn subscribe(&self) -> Result<Receiver<Bytes>> {
        let (tx, rx) = oneshot::channel();
        self.tx.send(tx).await?;

        let rx = rx.await?;
        Ok(rx)
    }
}

pub struct Orchestrator {
    pub name: String,
    client: etcd_client::Client,
    nodes: Arc<Mutex<HashMap<String, Node>>>,
}

impl Orchestrator {
    pub fn new(name: String, client: etcd_client::Client) -> Self {
        Self {
            name,
            client,
            nodes: Default::default(),
        }
    }

    pub async fn egress(&self, channel: String) -> Result<Receiver<Bytes>> {
        let uuid = Uuid::new_v4();

        let mut kv = self.client.kv_client();
        let mut lc = self.client.lease_client();

        let response = kv.get(format!("/ingress/{channel}"), None).await?;

        let mut lease_id = None;
        let mut location = response.kvs().first().and_then(|i| i.value_str().ok()).map(|s| s.to_owned());
        if location.is_none() {
            let header = response.header().unwrap();

            let lease = lc.grant(60, None).await?;
            lease_id = Some(lease.id());
            kv.put(
                format!("/barrier/{channel}/{uuid}"),
                "waiting",
                Some(PutOptions::new().with_lease(lease.id())),
            )
            .await?;

            tracing::info!("waiting");
            let (mut watcher, mut stream) = self
                .client
                .watch_client()
                .watch(
                    format!("/ingress/{channel}"),
                    WatchOptions::new()
                        .with_start_revision(header.revision())
                        .with_filters(vec![WatchFilterType::NoDelete])
                        .into(),
                )
                .await?;

            let ret = async {
                while let Some(update) = stream.try_next().await? {
                    let put = update
                        .events()
                        .iter()
                        .find(|e| e.event_type() == EventType::Put);
                    if let Some(e) = put {
                        let kv = e.kv().unwrap();
                        let value = kv.value_str()?;
                        return Ok(value.to_owned());
                    }
                }
                bail!("watch ended");
            }
            .await?;

            tracing::info!(?ret, "found on ");
            location = Some(ret);
            watcher.cancel().await?;
        }
        let location = location.unwrap();
        
        let rx = if location == self.name {
            tracing::info!("local");
            let nodes = self.nodes.lock().await;
            let node = nodes.get(&channel).unwrap();
            node.subscribe().await
        } else {
            tracing::info!(loc=location, "remote");
            let mut nodes = self.nodes.lock().await;
            if let Some(node) = nodes.get(&channel) {
                node.subscribe().await
            } else {
                let (input, sub) = Backend::spawn(channel.clone());
                let node = Node { tx: sub };
                let rx = node.subscribe().await;
                nodes.insert(channel.clone(), node);
                let client = reqwest::Client::new();
                let mut res = client.post(format!("http://localhost:8080/egress/{channel}")).send().await.unwrap();
                tracing::info!(?res, "remote post");
                tokio::spawn(async move {
                    while let Some(chunk) = res.chunk().await.unwrap() {
                        input.send(chunk).await;
                    }
                    tracing::info!("input eof");
                });

                rx
            }
        };

        if let Some(id) = lease_id {
            lc.revoke(id).await?;
        }
        rx
    }

    pub async fn ingress(&self, channel: String) -> Result<Input> {
        let (input, sub) = Backend::spawn(channel.clone());
        {
            let mut nodes = self.nodes.lock().await;
            nodes.insert(channel.clone(), Node { tx: sub });
        }

        let mut kv = self.client.kv_client();
        let mut watch = self.client.watch_client();
        kv.put(format!("/ingress/{channel}"), self.name.as_str(), None)
            .await?;
        tracing::info!("started");

        let barrier_initial = kv
            .get(
                format!("/barrier/{channel}/"),
                Some(GetOptions::new().with_prefix()),
            )
            .await?;
        tracing::info!(?barrier_initial, "barrier");
        let revision = barrier_initial.header().unwrap().revision();
        let mut barrier = barrier_initial.count();
        if barrier == 0 {
            tracing::info!("barrier cleared immediately");
            return Ok(input);
        } else {
            let (mut watch, mut stream) = watch
                .watch(
                    format!("/barrier/{channel}/"),
                    WatchOptions::new()
                        .with_prefix()
                        .with_start_revision(revision)
                        .into(),
                )
                .await?;
            while let Some(update) = stream.try_next().await? {
                for op in update.events() {
                    match op.event_type() {
                        EventType::Put => barrier += 1,
                        EventType::Delete => barrier -= 1,
                    }
                }
                tracing::info!(barrier, "update");
                if barrier == 0 {
                    break;
                }
            }
            watch.cancel().await?;
            tracing::info!("barrier cleared");
        }

        Ok(input)
    }
}
