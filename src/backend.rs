use bytes::Bytes;
use tokio::sync::{
    mpsc::{channel, Receiver, Sender},
    oneshot,
};
use tracing_futures::Instrument;

pub type Subscription = oneshot::Sender<Receiver<Bytes>>;

pub struct Input {
    tx: Sender<Bytes>,
}

impl Input {
    pub async fn send(&self, bytes: Bytes) {
        self.tx.send(bytes).await.unwrap();
    }
}

pub struct Backend {
    connections: Vec<Sender<Bytes>>,
    incoming: Receiver<Bytes>,
    subscriptions: Receiver<Subscription>,
}

impl Backend {
    async fn run(mut self) {
        loop {
            tokio::select! {
                Some(bytes) = self.incoming.recv() => {
                    for c in &self.connections {
                        let _ = c.send(bytes.clone()).await;
                    }
                }
                Some(sub) = self.subscriptions.recv() => {
                    let (tx, rx) = channel(1);
                    self.connections.push(tx);
                    let _ = sub.send(rx);
                }
                else => break
            }
        }
        tracing::info!("end");
    }
}

impl Backend {
    pub fn spawn(name: String) -> (Input, Sender<Subscription>) {
        let (tx_bytes, rx_bytes) = channel(1);
        let (tx_sub, rx_sub) = channel(1);

        tokio::spawn(
            async move {
                Backend {
                    connections: Vec::new(),
                    incoming: rx_bytes,
                    subscriptions: rx_sub,
                }
                .run()
                .await
            }
            .instrument(tracing::info_span!("backend", name)),
        );

        (Input { tx: tx_bytes }, tx_sub)
    }
}
