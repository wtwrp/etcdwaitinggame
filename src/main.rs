use std::{io, sync::Arc, time::Duration};

use axum::{
    body::{Body, StreamBody},
    extract::{BodyStream, Path, State},
    routing::post,
    Router, Server,
};
use bytes::Bytes;
use color_eyre::{Report, Result};
use etcd_client::{Client, DeleteOptions};
use tokio_stream::{wrappers::ReceiverStream, Stream, StreamExt};
use tracing_futures::Instrument;

use crate::orchestrator::Orchestrator;

mod backend;
mod orchestrator;

struct RequestContext {
    o: Orchestrator,
}

#[tracing::instrument(skip_all, fields(name=context.o.name))]
async fn http_ingress(
    State(context): State<Arc<RequestContext>>,
    Path(channel): Path<String>,
    mut body: BodyStream,
) {
    let input = context.o.ingress(channel).await.unwrap();

    while let Some(chunk) = body.try_next().await.unwrap() {
        input.send(chunk).await;
    }
    tracing::info!("ingress eof");
}

#[tracing::instrument(skip_all, fields(name=context.o.name))]
async fn http_egress(
    State(context): State<Arc<RequestContext>>,
    Path(channel): Path<String>,
) -> StreamBody<impl Stream<Item = io::Result<Bytes>>> {
    let output = context.o.egress(channel).await.unwrap();

    StreamBody::new(ReceiverStream::new(output).map(Result::Ok))
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    let mut c1 = Client::connect(["http://localhost:2379"], None).await?;
    c1.delete("/", Some(DeleteOptions::new().with_prefix()))
        .await?;

    let server1 = tokio::spawn(async move {
        let o = Orchestrator::new("o1".into(), c1);
        let ctx = Arc::new(RequestContext { o });
        let router = Router::with_state(ctx)
            .route("/ingress/:channel", post(http_ingress))
            .route("/egress/:channel", post(http_egress));

        let addr = "0.0.0.0:8080".parse().unwrap();
        Server::bind(&addr)
            .serve(router.into_make_service())
            .await?;

        Ok::<_, Report>(())
    });
    let server2 = tokio::spawn(async move {
        let c2 = Client::connect(["http://localhost:2379"], None).await?;
        let o = Orchestrator::new("o2".into(), c2);
        let ctx = Arc::new(RequestContext { o });
        let router = Router::with_state(ctx)
            .route("/ingress/:channel", post(http_ingress))
            .route("/egress/:channel", post(http_egress));

        let addr = "0.0.0.0:8081".parse().unwrap();
        Server::bind(&addr)
            .serve(router.into_make_service())
            .await?;

        Ok::<_, Report>(())
    });

    let i1 = tokio::spawn(async move {
        tokio::time::sleep(Duration::from_secs(2)).await;
        let http_client = reqwest::Client::new();
        let res = http_client
            .post("http://localhost:8080/ingress/tv")
            .body("hello123")
            .send()
            .await?;

        tracing::info!(status=?res.status(), "ingress");

        Ok::<_, Report>(())
    });

    let e1 = tokio::spawn(async move {
        tokio::time::sleep(Duration::from_secs(1)).await;
        let http_client = reqwest::Client::new();
        let mut res = http_client
            .post("http://localhost:8081/egress/tv")
            .body(Body::empty())
            .send()
            .await?;

        tracing::info!(status=?res.status(), "egress");

        while let Some(chunk) = res.chunk().await? {
            let s = std::str::from_utf8(&chunk)?;
            tracing::info!(v = s, "chunk")
        }

        Ok::<_, Report>(())
    }.instrument(tracing::info_span!("e1")));

    let (s1, s2, i1, e1) = tokio::try_join!(server1, server2, i1, e1).unwrap();
    (s1?, s2?, i1?, e1?);

    Ok(())
}
